{
    "name": "CG SCOP - Connecteur RIGA",
    "summary": "Connecteur RIGA",
    "version": "12.0.1.0.1",
    "development_status": "Beta",
    "author": "Le Filament",
    "license": "AGPL-3",
    "application": False,
    "depends": [
        'base',
        'web',
        'cgscop_partner',
        'cgscop_riga_import',
    ],
    "data": [
        "security/ir.model.access.csv",
        "views/riga_connection.xml",
        "views/riga_job_queue.xml",
        "datas/ir_config_parameter.xml",
        "datas/cron_odoo_riga.xml",
    ],
    'installable': True,
    'auto_install': False,
}
