# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from odoo import models, fields


class CgscopRigaConnection(models.Model):
    _name = 'riga.connection'
    _description = 'Infos API de connexion RIGA'

    name = fields.Char(string='Nom')
    login = fields.Char('Login')
    password = fields.Char('Password')
    url = fields.Char('URL')
    active = fields.Boolean('Actif', default=False)

    def toogle_active_config(self):
        """ Active la connexion sélectionnée et désactive
            toutes les autres connexions
        """
        for item in self.env['riga.connection'].search([]):
            item.active = False
        self.active = True
