# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from . import res_partner
from . import riga_api
from . import riga_connection
from . import riga_job_queue
