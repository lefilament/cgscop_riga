# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

import logging

from odoo import fields, models, api

_logger = logging.getLogger(__name__)


class CgscopRigaJobQueue(models.Model):
    """ Ce modèle permet de créer une liste d'attente d'envoi
    des organismes modifiés ou créés dans Odoo
    """
    _name = 'riga.job.queue'
    _inherit = 'riga.api'
    _description = 'Liste synchronisation RIGA'
    _rec_name = 'partner_id'
    _order = 'create_date desc'

    partner_id = fields.Many2one(
        comodel_name='res.partner',
        string='Organisme')
    id_riga = fields.Char('ID Riga')
    field = fields.Char('Champ modifié')
    old_value = fields.Char('Valeur précédente')
    new_value = fields.Char('Nouvelle valeur')
    date_sync = fields.Datetime('Dernière synchro',)
    action = fields.Selection(
        selection=[('create', 'Création'), ('update', 'Mise à jour')],
        string='Action')
    is_sync = fields.Boolean('Synchronisé', default=False)
    log = fields.Text('Log')
    is_error = fields.Boolean('Erreur Synchro', default=False)

    @api.multi
    def riga_sync(self):
        """ Synchronisation de Odoo vers RIGA
        """
        jobs = self.filtered(lambda j: not j.is_sync)
        # Filtre sur les créations pour les effectuer en premier
        jobs_create = jobs.filtered(lambda j: j.action == 'create')
        # Groupe les partenaires
        jobs_create_partner = jobs_create.mapped('partner_id')
        # Lance la création
        for partner in jobs_create_partner:
            job = jobs_create.filtered(lambda j: j.partner_id == partner)
            # Envoi à l'API
            self._send_api(
                partner=partner,
                job=job,
                creation=True)

        # Filtre sur les mise à jour
        jobs_update = jobs.filtered(lambda j: j.action == 'update')
        # Groupe les partenaires
        jobs_update_partner = jobs_update.mapped('partner_id')
        # Lance la mise à jour
        for partner in jobs_update_partner:
            job = jobs_update.filtered(lambda j: j.partner_id == partner)
            # Envoi à l'API
            self._send_api(
                partner=partner,
                job=job,
                creation=False)

    def _send_api(self, partner, job, creation):
        """ Premet d'envoyer un organisme à l'API

        :param partner (obj): organisme à envoyer à l'API
        :param job (obj): liste des job.queue associées au partenaire
        :param create (bool): défini si la fonction doit créer ou maj
        """
        # Crée l'objet à envoyer via l'API
        if creation:
            json = partner.riga_create_json()
        else:
            json = partner.riga_update_json()

        try:
        # Envoi de l'objet à l'API
            if creation:
                org = job.create_organism(json)
            else:
                org = job.update_organism(json)
            
            # Traitement de la réponse
            if org.get('pKey', False):
                # Retour de l'ID RIGA si création
                if creation:
                    partner.write({'id_riga': org.get('pKey')})
                    action = 'Création'
                else: 
                    action = 'Mise à jour'

                _logger.info(action + " de l'organisme " + partner.name +
                             " - ID RIGA : " + str(org.get('pKey')))
                # Log de l'action
                for j in job:
                    j.write({
                        'id_riga': org.get('pKey'),
                        'date_sync': fields.Datetime.now(),
                        'is_sync': True,
                        'is_error': False,
                        'log': org.get('apiMessage')})
            else:
                # Gestion de l'erreur API
                for j in job:
                    j.log = ("Erreur API : " + org.get('errorMessage') +
                            " (erreur " + str(org.get('errorNumber')) + ")")
                    j.is_error = True
                    j.date_sync = fields.Datetime.now()

        # Gestion de l'erreur Odoo
        except Exception as e:
            _logger.error(e.__str__())
            for j in job:
                j.log = "Erreur Odoo : " + str(e)
                j.is_error = True
                j.date_sync = fields.Datetime.now()

    def _cron_sync(self):
        jobs = self.env['riga.job.queue'].search([
            ['is_sync', '=', False]])
        return jobs.riga_sync()
