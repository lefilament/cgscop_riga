# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

import json

from odoo import models, api

TRACKED_FIELDS = ['name', 'street', 'street2', 'street3', 'zip',
    'city', 'cedex', 'website', 'phone', 'mobile', 'sigle', 'siret',
    'cae', 'email', 'facebook', 'twitter', 'partner_company_type_id',
    'naf_id', 'social_object', 'direccte_id', 'ur_id', 'creation_origin_id',
    'creation_suborigin_id', 'comment']


class RigaPartner(models.Model):
    _inherit = 'res.partner'

    # ------------------------------------------------------
    # Fonctions héritées
    # ------------------------------------------------------
    @api.multi
    def write(self, vals):
        """ Surcharge la fonction write() pour vérifier si il y
        a eu des changements sur les champs à pousser dans RIGA
        """
        Queue = self.env['riga.job.queue']
        tracked_fields = self.fields_get(TRACKED_FIELDS)
        
        # Dict des valeurs initiales des champs présents dans TRACKED_FIELDS
        initial_values = dict(
            (record.id, dict(
                (key, getattr(record, key)) for key in tracked_fields)) for record in self.filtered('is_cooperative'))
        
        # Ecriture des nouvelles valeurs
        result = super(RigaPartner, self).write(vals)
        
        # Dict des nouvelles valeurs
        new_values = dict(
            (record.id, dict(
                (key, getattr(record, key)) for key in tracked_fields)) for record in self.filtered('is_cooperative'))

        # Check des modifications sur les coopératives présentes dans RIGA
        for record in self:
            if record.is_cooperative and record.id_riga:
                for field in TRACKED_FIELDS:
                    if new_values[record.id][field] != initial_values[record.id][field]:
                        Queue.create({
                            'partner_id': record.id,
                            'action': 'update',
                            'field': field,
                            'old_value': str(initial_values[record.id][field]),
                            'new_value': str(new_values[record.id][field]),
                            'id_riga': record.id_riga
                        })

        return result

    @api.multi
    def scop_send_to_cg(self):
        """ Hérite la fonction d'envoi de l'organisme à la CG
        pour validation. Cette fonction passe le statut de l'organisme
        à l'état "Attente retour CG"

        @return : True
        """
        super(RigaPartner, self).scop_send_to_cg()
        if not self.id_riga:
            Queue = self.env['riga.job.queue']
            Queue.create({
                'partner_id': self.id,
                'action': 'create',
            })

        return True

    # ------------------------------------------------------
    # JSON pour création et maj RIGA
    # ------------------------------------------------------

    def riga_json(self):
        """ Retourne une base de JSON pour les fonctions
        create et update

        @return : objet json
        """
        self.ensure_one()
        riga_obj = {
            "raison_sociale": self.name,
            "rue1": self.street,
            "rue2": self._empty_field(self.street2),
            "rue3": self._empty_field(self.street3),
            "cp": self._empty_field(self.zip),
            "ville": self._empty_field(self.city),
            "cedex": self._empty_field(self.cedex),
            "site_web": self._empty_field(self.website),
            "tel": self._empty_field(self.phone),
            "fax": self._empty_field(self.mobile),
            "sigle": self._empty_field(self.sigle),
            "pays": "FRANCE",
            "siret": self._empty_field(self.siret),
            "cae": "1" if self.cae else "0",
            "etb_principal": "1",
            "mail_admin": self._empty_field(self.email),
            "facebook": self._empty_field(self.facebook),
            "twitter": self._empty_field(self.twitter),
            "forme_juridique": self._empty_field(self.partner_company_type_id.id_riga),
            "naf": self._empty_field(self.naf_id.id_riga),
            "objet_social": self._empty_field(self.social_object),
            "infoMvt": {
                "ur": self._empty_field(self.ur_id.id_riga),
                "origine_creation": self._empty_field(self.creation_origin_id.id_riga),
                "sous_origine_creation": self._empty_field(self.creation_suborigin_id.id_riga),
                "notes": self._empty_field(self.comment)
            }
        }
        return riga_obj

    def riga_update_json(self):
        """ Retourne l'objet attendu par l'API RIGA
        pour la mise à jour d'un organisme

        @return : objet json
        """
        self.ensure_one()
        riga_obj = self.riga_json()
        riga_obj.update({
            "pkey": self.id_riga,
        })
        return riga_obj

    def riga_create_json(self):
        """ Retourne l'objet attendu par l'API RIGA 
        pour la création d'un organisme

        @return : objet json
        """
        self.ensure_one()
        riga_obj = self.riga_json()
        riga_obj.update({
            "type": "11516",
            "sous_type": self._get_subtype(),
            "etat": "12817",
            "etb_principal": "1",
        })
        return riga_obj

    def _get_subtype(self):
        subtype = self.cooperative_form_id.name
        if subtype == 'SCOP':
            return "11544"
        elif subtype == 'SCIC':
            return "11543"
        elif subtype == 'Lamaneur':
            return "14214"
        else:
            return "14135"

    def _empty_field(self, field):
        if not field:
            return ''
        else:
            return str(field)
