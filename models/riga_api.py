# © 2019 Le Filament (<http://www.le-filament.com>)
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

import requests
import logging

from datetime import timezone

from odoo import fields, models, exceptions

_logger = logging.getLogger(__name__)


class CgscopRiga(models.AbstractModel):
    """ Cet Abstract Model partage l'ensemble des fonctions
    de l'API RIGA.
    """
    _name = 'riga.api'
    _description = 'RIGA abstract model'

    def get_by_url(self, url, call_type, data=False):
        _logger.info("Appel RIGA :  %s" % url)
        # Gestion du token de connexion
        param = self.env['ir.config_parameter'].sudo()
        date_token = fields.Datetime.to_datetime(
            param.get_param('riga.date_token')).replace(tzinfo=timezone.utc)
        now = fields.Datetime.context_timestamp(
            param,
            fields.Datetime.now()).replace(tzinfo=timezone.utc)
        # Si token NOK : création token
        if now > date_token and url != 'Api/Authentification/Connect':
            riga_connexion = self.connect()
            # Mise à jour des paramètres de connexion
            param.set_param('riga.token', riga_connexion.get('token'))
            date_token = fields.Datetime.to_datetime(
                riga_connexion.get('expirationDate').replace('/', '-'))
            param.set_param(
                'riga.date_token',
                date_token)

        # Sinon : Appel API
        max_try = int(param.get_param('riga.max_try'))
        token = param.get_param('riga.token')
        riga_param = self.env['riga.connection'].sudo().search([
            ['active', '=', True]])
        riga_url = riga_param.url
        for i in range(max_try):
            try:
                if call_type == 'get':
                    response = requests.get(riga_url + url)
                    break
                elif call_type == 'post':
                    json = dict({'token': token}, **data)
                    response = requests.post(
                        riga_url + url, json=json)
                    break
            except Exception as err:
                _logger.warning(
                    "Erreur appel URL %d/%d. \n URL: %s",
                    i, max_try, err.__str__(),
                )
        else:
            raise exceptions.Warning(
                'Les tentatives de connexion ont échoué')
        return response.json()

    def connect(self):
        """ Connect function
            :param login: utilisateur RIGA
            :param password: password utilisateur riga
            @return object: token
        """
        _logger.info("Création du Token de connexion RIGA")
        riga_param = self.env['riga.connection'].sudo().search([
            ['active', '=', True]])
        # Objet de connexion RIGA
        riga_connexion = requests.post(
            riga_param.url + 'Api/Authentification/Connect',
            json={
                "apiLogin": riga_param.login,
                "apiPassword": riga_param.password})
        return riga_connexion.json()

    def disconnect(self):
        """ Disconnect function
            @return object: données de l'organisme sous format json
        """
        param = self.env['ir.config_parameter'].sudo()
        token = param.get_param('riga.token')
        return self.get_by_url(
            url='Api/Authentification/Disconnect',
            call_type='post',
            data={
                'token': token,
            })

    def read_organism(self, id):
        """ Read function
            :param id: id RIGA de l'organisme
            @return object: données de l'organisme sous format json
        """
        return self.get_by_url(
            url='Api/Organisme/Read',
            call_type='post',
            data={"pKey_Organisme": id})

    def update_organism(self, record):
        """ Create function
            :param record: objet à mettre à jour
            @return object: résultat requête au format json
        """
        return self.get_by_url(
            url='Api/Organisme/Update',
            call_type='post',
            data={"record": record})

    def create_organism(self, record):
        """ Create function
            :param record: objet à créer
            @return int: id riga de l'objet créé
        """
        return self.get_by_url(
            url='Api/Organisme/Create',
            call_type='post',
            data={"record": record})
