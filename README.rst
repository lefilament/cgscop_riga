.. image:: https://img.shields.io/badge/licence-AGPL--3-blue.svg
   :target: http://www.gnu.org/licenses/agpl
   :alt: License: AGPL-3


=========================
CG SCOP - Connecteur RIGA
=========================

Description
===========

Ce module permet de synchroniser les partenaires (organismes, sociétés, contacts...) Odoo et RIGA.

Il utilise l'API RIGA fournie par la CG SCOP pour créer, mettre à jour ou récupérer les informations présentes dans RIGA. Chaque objet à modifier est mis dans une file d'attente, catte file d'attente est traitée une fois par jour et permet de logguer l'état de traitement de chaque objet.


Usage
=====

Pour configurer ce module, les données suivantes sont à renseigner dans le menu **Configuration > APIs CG Scop > RIGA > Configuration API** : 

* *riga_login* : login de connexion à l'API
* *riga_password* : password de connexion à l'API
* *riga_url* : url de l'API

Les données suivantes sont présentes dans la table **ir.config.parameter** :

* *riga_max_try* : nombre max d'essais de connexion
* *riga_token* : token de connexion
* *riga_date_token* : date d'expiration du token de connexion


Credits
=======

Funders
------------

The development of this module has been financially supported by:

    Confédération Générale des SCOP (https://www.les-scop.coop)


Contributors
------------

* Juliana Poudou <juliana@le-filament.com>
* Rémi Cazenave <remi@le-filament.com>
* Benjamin Rivier <benjamin@le-filament.com>


Maintainer
----------

.. image:: https://le-filament.com/images/logo-lefilament.png
   :alt: Le Filament
   :target: https://le-filament.com

This module is maintained by Le Filament
